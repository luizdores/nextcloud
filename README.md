## Nextcloud 
Esse docker-compose.yml tem como objetivo subir um container do:

 

 
- [Nextcloud](https://nextcloud.com/) 
 - Proxy nginx com certificação **SSL** gerada pelo [let's encrypt](https://letsencrypt.org/) 
-  Banco de dados [MariaDB](https://mariadb.org/)
-  Servidor [onlyoffice](https://www.onlyoffice.com/) com licença modificada que pode ser encontrado [aqui](https://github.com/aleho/onlyoffice-ce-docker-license)

Ele foi baseado [neste](https://blog.ssdnodes.com/blog/installing-nextcloud-docker/) tutorial com algumas adições minhas para conexão mais simples com o banco de dados e servidor externo do onlyoffice.


## Setup

Sua configuração é bem simples, vamos precisar alterar apenas os seguintes serviços

**MariaDB**

No bloco `db:` 

        db:
		  ...
		  environment:
			- MYSQL_ROOT_PASSWORD=senhaderootdobancodedados
			- MYSQL_PASSWORD=senhadobancodonextcloud
		    - MYSQL_DATABASE=bancoqueseráusadopelonextcloud
			- MYSQL_USER=usuarioqueseráusadopelonextcloud
		  restart: unless-stopped


**Nextcloud**

No bloco `app:` 
  

      app:
	      ...
    	  environment:
			- VIRTUAL_HOST=nextcloud.seudominio.com.br	             
			- LETSENCRYPT_HOST=nextcloud.seudominio.com.br
	        - LETSENCRYPT_EMAIL=seu@email.com
	        - MYSQL_PASSWORD=senhaquevocedefiniunoblocoacima
	        - MYSQL_DATABASE=bancodefinidoacima
	        - MYSQL_USER=usuariodefinidoacima
	        - MYSQL_HOST=db
	     restart: unless-stopped
**Onlyoffice**

No bloco `onlyoffice:` 

    onlyoffice:
		      ...
			  environment:
			    - VIRTUAL_HOST=onlyoffice.seudominio.com.br	
			    - LETSENCRYPT_HOST=onlyoffice.seudominio.com.br	
			    - LETSENCRYPT_EMAIL=seu@email.com
		      restart: unless-stopped

Após isso devemos ter nosso docker-compose.yml já configurado, antes de subirmos de fato temos que criar a rede que será utilizado por nosso containers para uma comunicação mais organizada entre eles usando seguinte comando

```
docker network create nextcloud_network
```
feito isso na pasta em que está nosso docker-compose.yml devemos rodar o seguinte comando

    docker-compose up -d
caso se faça necessário fazer um debug pode se rodar o comando sem o -d, assim rodando o docker-compose em modo verbose

    docker-compose up

